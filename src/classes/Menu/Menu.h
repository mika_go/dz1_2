//
// Created by Михаил Кочетков on 31/10/2018.
//

#ifndef DZ1_MENU_H
#define DZ1_MENU_H

#pragma once

#include <vector>
#include <iostream>
#include <string>

class Menu {
    int context = 0;
    std::vector<int> data;
    bool isCreate = false;
    bool isLoad = false;
public:
    void run();

    void help();
    void info();
    void infoPart1();
    void infoPart2();

    void printData();
};

void drawTable(std::ostream &out, const std::vector<std::string> &header,
                     const std::vector<std::vector<std::string>> &data);
std::vector<std::string> splitString(std::string str, std::string del);

#endif //DZ1_MENU_H
