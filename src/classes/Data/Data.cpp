//
// Created by Михаил Кочетков on 01/11/2018.
//

#include <iostream>
#include "Data.h"

using namespace std;

Data::Data() {
    for (int i = 0; i < 1024; ++i) {
        surname[i] = '\0';
        autoMark[i] = '\0';
        requiredFuelMark[i] = '\0';
    }
}

vector<string> Data::ToVectorString() {
    return {surname, to_string(autoMarkCode), autoMark, requiredFuelMark, to_string(enginePower),
            to_string(tankVolume), to_string(fuelLeft), to_string(oilVolume)};
}

vector<string> Data::VarNamesToVector() {
    return {"Surname", "Auto Mark Code", "Auto Mark", "Required Fuel Mark", "Engine Power",
            "Tank Volume", "Fuel Left", "Oil Volume"};
}

istream &operator>>(istream &is, Data &data) {
    cout << "Enter Surname: ";
    is >> data.surname;

    cout << "Enter Auto Mark Code(integer): ";
    is >> data.autoMarkCode;

    cout << "Enter Auto Mark: ";
    is >> data.autoMark;

    cout << "Enter Required Fuel Mark: ";
    is >> data.requiredFuelMark;

    cout << "Enter Engine Power(integer): ";
    is >> data.enginePower;

    cout << "Enter Tank Volume(integer): ";
    is >> data.tankVolume;

    cout << "Enter Fuel Left(float): ";
    is >> data.fuelLeft;

    cout << "Enter Oil Volume(float): ";
    is >> data.oilVolume;

    return is;
}