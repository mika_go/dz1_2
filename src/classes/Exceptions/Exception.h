//
// Created by Михаил Кочетков on 31/10/2018.
//

#ifndef DZ1_EXCEPTION_H
#define DZ1_EXCEPTION_H

#pragma once

#include <string>

class Exception {
public:
    virtual const std::string what() const = 0;
};


#endif //DZ1_EXCEPTION_H
