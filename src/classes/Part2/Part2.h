//
// Created by Михаил Кочетков on 01/11/2018.
//

#ifndef DZ1_PART2_H
#define DZ1_PART2_H

#pragma once

#include "../Data/Data.h"

#include <map>

class Part2 {
public:
    static void Add(const Data &data);
    static void Edit(int id, const Data &data);
    static void Delete(int id);
    static void Clear();
    static void Find(std::map<std::string, std::string> flags);
    static std::vector<Data> FetchAll();
    static void ResultList(const std::string &to = "src/files/part2/result.txt",
                           const std::string &from = "src/files/part2/data.bin");
};

void drawTableInFile(std::ofstream &file, const std::vector<std::string> &header,
                     const std::vector<std::vector<std::string>> &data);

#endif //DZ1_PART2_H
