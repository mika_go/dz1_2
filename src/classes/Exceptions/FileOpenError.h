//
// Created by Михаил Кочетков on 31/10/2018.
//

#ifndef DZ1_FILEOPENERROR_H
#define DZ1_FILEOPENERROR_H

#pragma once

#include "Exception.h"

class FileOpenError : public Exception {
    std::string _fileName;
public:
    FileOpenError(std::string fileName);
    const std::string what() const override;
};


#endif //DZ1_FILEOPENERROR_H
