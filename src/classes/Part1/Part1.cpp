//
// Created by Михаил Кочетков on 29/10/2018.
//

#include "Part1.h"
#include "../Exceptions/FileOpenError.h"

#include <vector>
#include <random>
#include <algorithm>

using namespace std;

void Part1::CreateData() {
    mt19937 gen;
    gen.seed(static_cast<unsigned int>(time(nullptr)));
    uniform_int_distribution<> uid(-50, 50);

    ofstream file("src/files/part1/data.txt", ios::trunc);

    if (!file.is_open()) {
        throw FileOpenError("src/files/part1/data.txt");
    }

    for (int i = 0; i < 100; ++i) {
        file << uid(gen) << endl;
    }

    file.close();
}

void Part1::CreateDataByIter() {
    mt19937 gen;
    gen.seed(static_cast<unsigned int>(time(nullptr)));
    uniform_int_distribution<> uid(-50, 50);

    ofstream file("src/files/part1/data.txt", ios::trunc);

    if (!file.is_open()) {
        throw FileOpenError("src/files/part1/data.txt");
    }

    vector<int> nums(100);
    generate(nums.begin(), nums.end(), [&uid, &gen]() { return uid(gen);});

    copy(nums.begin(), nums.end(), ostream_iterator<int>(file, "\n"));

    file.close();
}


void Part1::Modify() {
    ifstream inFile("src/files/part1/data.txt");
    ofstream outFile("src/files/part1/result_data.txt");

    if (!inFile.is_open()) {
        throw FileOpenError("src/files/part1/data.txt");
    }

    if (!outFile.is_open()) {
        throw FileOpenError("src/files/part1/result_data.txt");
    }

    int sum = 0;

    for_each(istream_iterator<int>(inFile), istream_iterator<int>(), [&sum](const auto &item) {
        if (item < 0) {
            sum += item;
        }
    });

    sum /= 2;
    inFile.close();

    inFile.open("src/files/part1/data.txt");

    if (!inFile.is_open()) {
        throw FileOpenError("src/files/part1/data.txt");
    }

    transform(istream_iterator<int>(inFile), istream_iterator<int>(), ostream_iterator<int>(outFile, "\n"),
            [sum](const auto &item) {
        return item + sum;
    });

    inFile.close();
    outFile.close();
}