//
// Created by Михаил Кочетков on 01/11/2018.
//

#include <fstream>
#include <iomanip>
#include <iostream>
#include "Part2.h"
#include "../Exceptions/FileOpenError.h"

using namespace std;

void Part2::Add(const Data &data) {
    ofstream file("src/files/part2/data.bin", ios::app | ios::binary);

    if (!file.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    file.write((char*)&data, sizeof(data));
    file.close();
}

void Part2::Edit(int id, const Data &data) {
    ifstream fileIn("src/files/part2/data.bin", ios::binary);

    if (!fileIn.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    int idx = 0;
    int findIdx = -1;
    map<int, Data> items;
    Data temp{};

    while (fileIn.read((char*)&temp, sizeof(temp))) {
        items[idx] = temp;
        if (idx == id) {
            findIdx = idx;
        }
        ++idx;
    }

    fileIn.close();

    if (findIdx == -1) {
        return;
    }

    items[findIdx] = data;

    ofstream fileOut("src/files/part2/data.bin", ios::trunc | ios::binary);
    if (!fileOut.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    for (const auto &item : items) {
        fileOut.write((char*)&item.second, sizeof(item.second));
    }

    fileOut.close();
}

void Part2::Delete(int id) {
    ifstream fileIn("src/files/part2/data.bin", ios::binary);

    if (!fileIn.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    int idx = 0;
    int findIdx = -1;
    map<int, Data> items;
    Data temp{};

    while (fileIn.read((char*)&temp, sizeof(temp))) {
        items[idx] = temp;
        if (idx == id) {
            findIdx = idx;
        }
        ++idx;
    }

    fileIn.close();

    if (findIdx == -1) {
        return;
    }

    items.erase(findIdx);

    ofstream fileOut("src/files/part2/data.bin", ios::trunc | ios::binary);
    if (!fileOut.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    for (const auto &item : items) {
        fileOut.write((char*)&item.second, sizeof(item.second));
    }

    fileOut.close();
}

void Part2::ResultList(const string &to, const string &from) {
    ofstream result(to, ios::trunc);
    ifstream file(from, ios::binary);

    if (!file.is_open()) {
        throw FileOpenError(from);
    }

    if (!result.is_open()) {
        throw FileOpenError(to);
    }

    auto header = Data::VarNamesToVector();
    vector<vector<string>> data;
    Data temp{};
    double sum = 0.0;

    while (file.read((char*)&temp, sizeof(temp))) {
        sum += (double(temp.tankVolume) - temp.fuelLeft) * 2.5;
        data.emplace_back(temp.ToVectorString());
    }

    file.close();

    data.push_back({"", "", "", "", "", "Full fuel cost = ", to_string(sum) + "$", ""});
    drawTableInFile(result, header, data);

    result.close();
}

vector<Data> Part2::FetchAll() {
    ifstream file("src/files/part2/data.bin", ios::binary);

    if (!file.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    file.seekg(0, ios::end);

    if (file.tellg() == 0) {
        file.close();
        return {};
    }

    file.seekg(0, ios::beg);

    vector<Data> data;
    Data temp;

    while (file.read((char*)&temp, sizeof(temp))) {
        data.emplace_back(temp);
    }

    file.close();
    return data;
}

void Part2::Clear() {
    ofstream file("src/files/part2/data.bin", ios::binary);

    if (!file.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    file.clear();
    file.close();
}

void Part2::Find(std::map<std::string, std::string> flags) {
    ifstream file("src/files/part2/data.bin", ios::binary);

    if (!file.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    auto header = Data::VarNamesToVector();
    vector<Data> data;
    Data temp{};

    while (file.read((char*)&temp, sizeof(temp))) {
        bool isMatch = true;

        for (const auto &item : flags) {
            if (item.first == "Auto Mark" && item.second != temp.autoMark) {
                isMatch = false;
                break;
            }

            if (item.first == "Required Fuel Mark" && item.second != temp.requiredFuelMark) {
                isMatch = false;
                break;
            }

            if (item.first == "Engine Power" && stoi(item.second) != temp.enginePower) {
                isMatch = false;
                break;
            }
        }

        if (isMatch) {
            data.emplace_back(temp);
        }
    }

    file.close();

    ofstream fileOut("src/files/part2/find.bin", ios::trunc | ios::binary);

    if (!fileOut.is_open()) {
        throw FileOpenError("src/files/part2/data.bin");
    }

    for (const auto &item : data) {
        fileOut.write((char*)&item, sizeof(item));
    }

    fileOut.close();

    ResultList("src/files/part2/find.txt", "src/files/part2/find.bin");
}

void drawTableInFile(ofstream &file, const vector<string> &header, const vector<vector<string>> &data) {
    auto drawLine = [&file](vector<int> maxColWidth) {
        file << "+—";
        for (int i = 0; i < maxColWidth.size(); i++) {
            for (int j = 0; j < maxColWidth[i]; j++) {
                file << "—";
            }
            if (i != maxColWidth.size() - 1) {
                file << "—+—";
            }
        }
        file << "—+";
        file << endl;
    };

    auto drawTable = [&file, drawLine](vector<string> header, vector<vector<string>> data) {
        int countOfCols;
        bool isHeaderOn = true;
        if (int(header.size()) != 0) {
            countOfCols = int(header.size());
        } else {
            isHeaderOn = false;
            countOfCols = int(data[0].size());
        }

        vector<int> maxSizeInCol;
        for (int i = 0; i < countOfCols; i++) {
            int len = -1;
            for (int j = 0; j < data.size(); j++) {
                if (int(data[j][i].length()) > len) {
                    len = int(data[j][i].length());
                }
            }
            maxSizeInCol.emplace_back(len);
        }

        if (isHeaderOn) {
            for (int i = 0; i < countOfCols; i++) {
                if (int(header[i].length()) > maxSizeInCol[i]) {
                    maxSizeInCol[i] = int(header[i].length());
                }
            }


            drawLine(maxSizeInCol);
            file << "|";
            for (int i = 0; i < countOfCols; i++) {
                file << " ";
                file << setfill(' ') << right << setw(maxSizeInCol[i]) << header[i];
                file << " |";
            }
            file << endl;
        }

        drawLine(maxSizeInCol);
        for (int i = 0; i < data.size(); i++) {
            file << "|";
            for (int j = 0; j < countOfCols; j++) {
                file << " ";
                file << setfill(' ') << right << setw(maxSizeInCol[j]) << data[i][j];
                file << " |";
            }
            file << endl;
            if (i != data.size() - 1) {
                drawLine(maxSizeInCol);
            }
        }
        drawLine(maxSizeInCol);
    };

    vector<vector<string>> splitedData;

    for (const auto &item : data) {
        splitedData.push_back(item);
    }

    drawTable(header, splitedData);
}
