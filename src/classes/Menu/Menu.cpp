//
// Created by Михаил Кочетков on 31/10/2018.
//

#include <iostream>
#include <vector>
#include <iomanip>
#include "Menu.h"
#include "../Part1/Part1.h"
#include "../Part2/Part2.h"

using namespace std;

void Menu::run() {
    info();
    for (;;) {
        string buf;
        int answer;

        if (context == 0) {
            cout << "main";
        } else if (context == 1) {
            cout << "part 1";
        } else if (context == 2) {
            cout << "part 2";
        }

        cout << " > ";
        cin >> buf;

        if (buf == "\\h") {
            help();
            continue;
        }

        try {
            answer = stoi(buf);
        } catch (...) {
            cout << ">>> Type input error. Please, enter integer values <<<" << endl;
            continue;
        }

        if (answer == 0) {
            cout << "Exit" << endl;
            break;
        }

        if (context == 0) {
            if (answer == 1) {
                infoPart1();
                context = 1;
            } else if (answer == 2) {
                infoPart2();
                context = 2;
            } else {
                cout << "This command is not exist. Enter \\h to get info about current context" << endl;
            }
        } else if (context == 1) {
            try {
                string res;
                bool success = false;
                if (answer == -1) {
                    context = 0;
                    res = "back";
                    success = true;
                } else if (answer == 1) {
                    Part1::CreateData();
                    res = "data created";
                    isCreate = true;
                    success = true;
                } else if (answer == 2) {
                    Part1::CreateData();
                    res = "data created";
                    isCreate = true;
                    success = true;
                } else if (answer == 3 && isCreate) {
                    data.clear();
                    Part1::LoadData(data);
                    res = "data loaded";
                    isLoad = true;
                    success = true;
                } else if (answer == 4 && isCreate) {
                    data.clear();
                    Part1::LoadDataByIter(data);
                    res = "data loaded";
                    isLoad = true;
                    success = true;
                } else if (answer == 5 && isLoad) {
                    Part1::ResultData(data);
                    res = "data saved";
                    success = true;
                } else if (answer == 6 && isLoad) {
                    Part1::ResultDataByIter(data);
                    res = "data saved";
                    success = true;
                } else if (answer == 7 && isLoad) {
                    Part1::Modify(data);
                    res = "data modified";
                    success = true;
                } else if (answer == 8 && isLoad) {
                    Part1::Modify(data.begin(), data.end());
                    res = "data modified";
                    success = true;
                } else if (answer == 9 && isLoad) {
                    Operation operation;
                    Part1::ModifyByForEach(data, operation);
                    res = "data modified";
                    success = true;
                } else if (answer == 10 && isCreate) {
                    res = "data modified";
                    Part1::Modify();
                    success = true;
                } else if (answer == 11 && isLoad) {
                    printData();
                    res = "data printed";
                    success = true;
                }
                if (success) {
                    cout << "-- Success: " << res << " --" << endl;
                } else {
                    if (!isCreate) {
                        cout << "-- Fail: data is not created" << endl;
                    } else if (!isLoad) {
                        cout << "-- Fail: data is not loaded" << endl;
                    } else {
                        cout << "This command is not exist. Enter \\h to get info about current context" << endl;
                    }
                }
            } catch (const Exception &e) {
                cout << ">>> " << e.what() << " <<<" << endl;
            }
        } else if (context == 2) {
            try {
                string res;
                bool success = false;
                if (answer == -1) {
                    context = 0;
                    res = "back";
                    success = true;
                } else if (answer == 1) {
                    auto data = Part2::FetchAll();
                    if (!data.empty()) {
                        vector<vector<string>> dataToTable;
                        auto header = Data::VarNamesToVector();
                        header.insert(header.begin(), "Num");

                        for (int i = 0; i < data.size(); ++i) {
                            auto temp = data[i].ToVectorString();
                            temp.insert(temp.begin(), to_string(i));
                            dataToTable.emplace_back(temp);
                        }

                        drawTable(cout, header, dataToTable);
                    } else {
                        cout << "Data is empty" << endl;
                    }
                    res = "show";
                    success = true;
                } else if (answer == 2) {
                    Data data;

                    cout << "\tEnter Data" << endl;
                    cin >> data;

                    Part2::Add(data);
                    res = "add";
                    success = true;
                } else if (answer == 3) {
                    int id;
                    cout << "Choose id: ";
                    cin >> id;

                    Data d;

                    cout << "\tEnter new data" << endl;
                    cin >> d;
                    Part2::Edit(id, d);
                    res = "edit";
                    success = true;
                } else if (answer == 4) {
                    int id;
                    cout << "Choose id: ";
                    cin >> id;

                    Part2::Delete(id);
                    res = "delete";
                    success = true;
                } else if (answer == 5) {
                    map<string, string> flags;
                    string buffer;
                    auto varNames = Data::VarNamesToVector();

                    cout << "\tChoose parameters to search(like a 1,3,5)" << endl;

                    for (int i = 2; i < varNames.size() - 3; ++i) {
                        cout << i - 2 << ". " << varNames[i] << endl;
                    }

                    cin >> buffer;

                    auto splited = splitString(buffer, ",");

                    for (const auto &item : splited) {
                        int idx = stoi(item) + 2;
                        cout << "Enter " << varNames[idx] << ": ";
                        cin >> buffer;

                        flags[varNames[idx]] = buffer;
                    }

                    Part2::Find(flags);
                    res = "find";
                    success = true;
                } else if (answer == 6) {
                    Part2::ResultList();
                    res = "generate result file";
                    success = true;
                } else if (answer == 7) {
                    Part2::Clear();
                    res = "clear";
                    success = true;
                }
                if (success) {
                    cout << "-- Success: " << res << " --" << endl;
                } else {
                    cout << "This command is not exist. Enter \\h to get info about current context" << endl;
                }
            } catch (const Exception &e) {
                cout << ">>> " << e.what() << " <<<" << endl;
            }
        }
    }
}

void Menu::info() {
    cout << "Press 0 to exit" << endl;
    cout << "Press 1 to test first part" << endl;
    cout << "Press 2 to test second part" << endl;
    cout << "Enter \\h to get info" << endl;
}

void Menu::infoPart1() {
    cout << "Press -1 to go back" << endl;
    cout << "Press 0 to exit" << endl;
    cout << "Press 1 to create data" << endl;
    cout << "Press 2 to create data by iterators" << endl;
    cout << "Press 3 to load data" << endl;
    cout << "Press 4 to load data by iterators" << endl;
    cout << "Press 5 to save data" << endl;
    cout << "Press 6 to save data by iterators" << endl;
    cout << "Press 7 to modify data by template container" << endl;
    cout << "Press 8 to modify data by iterators" << endl;
    cout << "Press 9 to modify data by for_each" << endl;
    cout << "Press 10 to modify data by transform" << endl;
    cout << "Press 11 to print data" << endl;
    cout << "Enter \\h to get info" << endl;
}

void Menu::infoPart2() {
    cout << "Press -1 to go back" << endl;
    cout << "Press 0 to exit" << endl;
    cout << "Press 1 to show data" << endl;
    cout << "Press 2 to add new data" << endl;
    cout << "Press 3 to edit data by id" << endl;
    cout << "Press 4 to delete data by id" << endl;
    cout << "Press 5 to find data" << endl;
    cout << "Press 6 to generate result file" << endl;
    cout << "Press 7 to clear data" << endl;
    cout << "Enter \\h to get info" << endl;
}

void Menu::help() {
    if (context == 0) {
        info();
    } else if (context == 1) {
        infoPart1();
    } else if (context == 2) {
        infoPart2();
    }
}

void Menu::printData() {
    cout << "[ ";
    for (int i = 0; i < data.size(); ++i) {
        if (i != 0 && i % 30 == 0) {
            cout << endl;
        }
        cout << data[i];
        if (i != data.size() - 1) {
            cout << ", ";
        }
    }
    cout << " ]" << endl;
}

void drawTable(ostream &out, const vector<string> &header, const vector<vector<string>> &data) {
    auto drawLine = [&out](vector<int> maxColWidth) {
        out << "+—";
        for (int i = 0; i < maxColWidth.size(); i++) {
            for (int j = 0; j < maxColWidth[i]; j++) {
                out << "—";
            }
            if (i != maxColWidth.size() - 1) {
                out << "—+—";
            }
        }
        out << "—+";
        out << endl;
    };

    auto drawTable = [&out, drawLine](vector<string> header, vector<vector<string>> data) {
        int countOfCols;
        bool isHeaderOn = true;
        if (int(header.size()) != 0) {
        countOfCols = int(header.size());
    } else {
        isHeaderOn = false;
        countOfCols = int(data[0].size());
    }

        vector<int> maxSizeInCol;
        for (int i = 0; i < countOfCols; i++) {
            int len = -1;
            for (int j = 0; j < data.size(); j++) {
                if (int(data[j][i].length()) > len) {
                    len = int(data[j][i].length());
                }
            }
            maxSizeInCol.emplace_back(len);
        }

        if (isHeaderOn) {
            for (int i = 0; i < countOfCols; i++) {
                if (int(header[i].length()) > maxSizeInCol[i]) {
                    maxSizeInCol[i] = int(header[i].length());
                }
            }


            drawLine(maxSizeInCol);
            out << "|";
            for (int i = 0; i < countOfCols; i++) {
                out << " ";
                out << setfill(' ') << right << setw(maxSizeInCol[i]) << header[i];
                out << " |";
            }
            out << endl;
        }

        drawLine(maxSizeInCol);
        for (int i = 0; i < data.size(); i++) {
            out << "|";
            for (int j = 0; j < countOfCols; j++) {
                out << " ";
                out << setfill(' ') << right << setw(maxSizeInCol[j]) << data[i][j];
                out << " |";
            }
            out << endl;
            if (i != data.size() - 1) {
                drawLine(maxSizeInCol);
            }
        }
        drawLine(maxSizeInCol);
    };

    vector<vector<string>> splitedData;

    for (const auto &item : data) {
        splitedData.push_back(item);
    }

    drawTable(header, splitedData);
}

vector<string> splitString(string str, string del) {
    vector<string> result;
    while(!str.empty()){
        unsigned long index = static_cast<int>(str.find(del));
        if (index != string::npos) {
            result.push_back(str.substr(0, index));
            str = str.substr(index + del.length());
            if (str.empty()) {
                result.push_back(str);
            }
        }else{
            result.push_back(str);
            str = "";
        }
    }
    return result;
}