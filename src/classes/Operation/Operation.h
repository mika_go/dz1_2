//
// Created by Михаил Кочетков on 11/11/2018.
//

#ifndef DZ1_OPERATION_H
#define DZ1_OPERATION_H


class Operation {
public:
    int sum;
    int variant;

    void operator()(int &);
};


#endif //DZ1_OPERATION_H
