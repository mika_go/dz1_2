//
// Created by Михаил Кочетков on 11/11/2018.
//

#include "Operation.h"

void Operation::operator()(int &item) {
    if (variant == 0) {
        if (item < 0) {
            sum += item;
        }
    } else {
        item += sum;
    }
}
